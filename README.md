# demos

demo videos for various projects

## Clouds

my BSc project, cloud simulation and visualisation

[code here](https://gitlab.com/jonebone/bsc_project.git)

[ui video](https://youtu.be/fqxdiS7Mlfk)

[full run video](https://youtu.be/_I-1nu1IHzw)

[running for a while ](https://youtu.be/8QoMvRY8cn8) this one uses a 64x64x64 noise texture

[shadertoy demo](https://www.shadertoy.com/view/wtGfDm) 



## simulations

 work for the masters level Animation and Simulation course

 [code here](https://gitlab.com/jonebone/animation) 

#### cloth sim

[video](https://youtube.com/shorts/iLelhND1ZUw?feature=share)

#### fluid sim

[video](https://youtube.com/shorts/fyg-z-KYg34?feature=share)


## renderers

work for the masters course foundations of modeling and rendering

[code here](https://gitlab.com/jonebone/rendering_modeling.git)


#### FakeGL

a recreation of OpenGL's fixed function pipleline

[video](https://youtu.be/v0XWOApYwi0)

#### raytracer

running a raytracer on CPU is not fast, so the demo here is limited

[video](https://youtu.be/jCw11NbVfsE)



